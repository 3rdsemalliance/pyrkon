#ifndef MAINH
#define MAINH
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include "util.h"
/* boolean */
#define TRUE 1
#define FALSE 0
#define SEC_IN_STATE 1
#define STATE_CHANGE_PROB 10

#define ROOT 0

/* tutaj TYLKO zapowiedzi - definicje w main.c */
extern int rank;
extern int size;
extern int ackCount;
extern pthread_t threadKom;
extern int lamport;
extern int edition;

// zmienne definiujące liczbę biletów, warsztatów, miejsc itp.
extern int ticketCount;
extern int workshopCount;
extern int workshopMemberCount;
// zmienne definiujące tablice uczestników oczekujących na bilet oraz ich liczbę
extern int* TicketWaiting;
extern int* WorkshopWaiting;

extern int TicketWaiterNum;
extern int WorkshopWaiterNum;
// zmienna przechowująca identyfikatory warsztatów, które proces chce odwiedzić
extern int* WantedWorkshops;
// oraz ich liczba
extern int wantedWorkshopCount;
// zmienna określająca warsztat, na który w danej chwili próbuje dostać się dany proces
extern int currentlySoughtAfter;
// zmienne przechowujące liczby odpowiedzi na dane komunikaty
extern int TicketAckNum; // ile dostaliśmy TICKET_ACK
extern int WorkshopAckNum; // ile dostaliśmy WORKSHOP_ACK
extern int EndNum; // ile dostaliśmy END_YES
extern int EndResp; // ile dostaliśmy END_YES lub END_NO
// zmienna określająca powstanie (lub nie) nowej edycji Pyrkonu
extern int newPyrkon;
// semafor na licznik procesów na Pyrkonie - to wyleci, jest do debugu
extern pthread_mutex_t licznikMut;
// semafor na liczniku lamporta
extern pthread_mutex_t lamportMut;

// semafor dla TicketAcków

// semafor dla WorkshopAcków

// tablica przechowująca wartości licznika lamporta w momencie wysyłania żądania dostępu do Pyrkonu do danego procesu
extern int *pyrkonLamportValues;
// i to samo dla warsztatu, o który się proces ubiega
extern int *workshopLamportValues;

/* macro debug - działa jak printf, kiedy zdefiniowano
   DEBUG, kiedy DEBUG niezdefiniowane działa jak instrukcja pusta 
   
   używa się dokładnie jak printfa, tyle, że dodaje kolorków i automatycznie
   wyświetla rank

   w związku z tym, zmienna "rank" musi istnieć.

   w printfie: definicja znaku specjalnego "%c[%d;%dm [%d]" escape[styl bold/normal;kolor [RANK]
                                           FORMAT:argumenty doklejone z wywołania debug poprzez __VA_ARGS__
					   "%c[%d;%dm"       wyczyszczenie atrybutów    27,0,37
                                            UWAGA:
                                                27 == kod ascii escape. 
                                                Pierwsze %c[%d;%dm ( np 27[1;10m ) definiuje styl i kolor literek
                                                Drugie   %c[%d;%dm czyli 27[0;37m przywraca domyślne kolory i brak pogrubienia (bolda)
                                                ...  w definicji makra oznacza, że ma zmienną liczbę parametrów
                                            
*/
#ifdef DEBUG
#define debug(FORMAT,...) printf("%c[%d;%dm [%d][LAMPORT=%d]: " FORMAT "%c[%d;%dm\n",  27, (1+(rank/7))%2, 31+(6+rank)%7, rank, lamport, ##__VA_ARGS__, 27,0,37);
#else
#define debug(...) ;
#endif

// makro println - to samo co debug, ale wyświetla się zawsze
#define println(FORMAT,...) printf("%c[%d;%dm [%d][LAMPORT=%d]: " FORMAT "%c[%d;%dm\n",  27, (1+(rank/7))%2, 31+(6+rank)%7, rank, lamport, ##__VA_ARGS__, 27,0,37);


#endif
