#include "main.h"
#include "watek_komunikacyjny.h"

/* wątek komunikacyjny; zajmuje się odbiorem i reakcją na komunikaty */
void *startKomWatek(void *ptr)
{
    MPI_Status status;
    int is_message = FALSE;
    //pthread_mutex_t lamportMut = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t ticketMut = PTHREAD_MUTEX_INITIALIZER;
    packet_t pakiet;
    /* Obrazuje pętlę odbierającą pakiety o różnych typach */
    int flag = 1, ticketPriorytet, workshopPriorytet;
    while ( flag ) {
	debug("czekam na recv");
        MPI_Recv( &pakiet, 1, MPI_PAKIET_T, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        pthread_mutex_lock(&lamportMut);
        ticketPriorytet = 0, workshopPriorytet = 0;
        if(pakiet.ts < pyrkonLamportValues[status.MPI_SOURCE])
            ticketPriorytet = 1;
        if(pakiet.ts < workshopLamportValues[status.MPI_SOURCE])
            workshopPriorytet = 1;
        if(lamport < pakiet.ts){
            lamport = pakiet.ts;
            //priorytet = 1;
        }
        ++lamport;
        pthread_mutex_unlock(&lamportMut);
        switch ( status.MPI_TAG ) {
	    case TICKET_REQ:
                debug("%d chce wejść na Pyrkon.", status.MPI_SOURCE)
                if(stan == WaitForPyrkon || (stan == TicketWait && ticketPriorytet) || stan == OffPyrkon){
                    debug("W sumie nie jestem na Pyrkonie, niech ma.");
		    sendPacket( 0, status.MPI_SOURCE, TICKET_ACK );
                }
                else{
                    if(stan == OnPyrkon || stan == WorkshopWait || stan == OnWorkshop)
                        debug("Ale ja już jestem na Pyrkonie, wara!");
                    if(stan == TicketWait && !ticketPriorytet)
                        debug("Ale ja czekam od %d, a on dopiero się zgłosił o %d!", pyrkonLamportValues[status.MPI_SOURCE], pakiet.ts);
                    pthread_mutex_lock(&ticketMut);
                    ++TicketWaiterNum;
                    TicketWaiting = (int*)realloc(TicketWaiting, TicketWaiterNum);
                    TicketWaiting[TicketWaiterNum-1] = status.MPI_SOURCE;
                    debug("No to mam już %d w kolejce, a oto oni:", TicketWaiterNum);
                    for(int i = 0; i < TicketWaiterNum; ++i){
                        debug("%d: %d", i + 1, TicketWaiting[i]);
                    }
                    pthread_mutex_unlock(&ticketMut);
                }
	    break;
	    case TICKET_ACK:
                ++TicketAckNum;
                debug("Dostałem TICKET_ACK od %d, mam już %d", status.MPI_SOURCE, TicketAckNum);
	        //TicketAckNum++; /* czy potrzeba tutaj muteksa? Będzie wyścig, czy nie będzie? Zastanówcie się. */
	    break;
            case WORKSHOP_REQ:
                //println("Dostałem workshop req od %d na warsztat %d", status.MPI_SOURCE, pakiet.data);
                debug("%d chce na warsztat %d.", status.MPI_SOURCE, pakiet.data);
                if((stan == OnWorkshop && pakiet.data == WantedWorkshops[currentlySoughtAfter]) || (stan == WorkshopWait && pakiet.data == WantedWorkshops[currentlySoughtAfter] && !workshopPriorytet)){
                    if(stan == OnWorkshop && pakiet.data == WantedWorkshops[currentlySoughtAfter])
                        debug("Ale ja jestem na tym warsztacie, wara!");
                    if(stan == WorkshopWait && pakiet.data == WantedWorkshops[currentlySoughtAfter] && !workshopPriorytet)
                        debug("Ale ja czekam od %d, a on zgłosił się dopiero o %d!", workshopLamportValues[status.MPI_SOURCE], pakiet.ts);
                    pthread_mutex_lock(&ticketMut);
                    ++WorkshopWaiterNum;
                    WorkshopWaiting = (int*)realloc(WorkshopWaiting, WorkshopWaiterNum);
                    WorkshopWaiting[WorkshopWaiterNum-1] = status.MPI_SOURCE;
                    debug("No to mam już %d w kolejce, a oto oni:", WorkshopWaiterNum);
                    for(int i = 0; i < WorkshopWaiterNum; ++i){
                        debug("%d: %d", i + 1, WorkshopWaiting[i]);
                    }
                    pthread_mutex_unlock(&ticketMut);
                }
                else{
                    debug("W sumie to mnie nie interesuje. Niech ma.");
                    sendPacket( 0, status.MPI_SOURCE, WORKSHOP_ACK );
                }
            break;
            case WORKSHOP_ACK:
                ++WorkshopAckNum;
                debug("Dostałem WORKSHOP_ACK od %d, mam już %d", status.MPI_SOURCE, WorkshopAckNum);
            break;
            case END_REQ:
                debug("Czy już skończyłem?");
                if(stan == OffPyrkon){
                    debug("Tak!");
                    sendPacket( 0, status.MPI_SOURCE, END_YES );
                }
                else{
                    debug("Nie!");
                    sendPacket( 0, status.MPI_SOURCE, END_NO );
                }
            break;
            case END_YES:
                if(stan == OffPyrkon)
                    ++EndNum;
            case END_NO:
                if(stan == OffPyrkon)
                    ++EndResp;
            break;
            case START_PYRKON:
                if(stan == OffPyrkon){
                    debug("Zaczynamy nowy Pyrkon!");
                    newPyrkon = 1;
                }
	    default:
	    break;
        }
    }
}
