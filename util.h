#ifndef UTILH
#define UTILH
#include "main.h"

/* typ pakietu */
typedef struct {
    int ts;       /* timestamp (zegar lamporta */
    int src;  

    int data;     /* przykładowe pole z danymi; można zmienić nazwę na bardziej pasującą */
} packet_t;
/* packet_t ma trzy pola, więc NITEMS=3. Wykorzystane w inicjuj_typ_pakietu */
#define NITEMS 3

/* Typy wiadomości */
/* TYPY PAKIETÓW */
#define TICKET_REQ   1
#define TICKET_ACK   2
#define WORKSHOP_REQ 3
#define WORKSHOP_ACK 4
#define END_REQ      5
#define END_YES      6
#define END_NO       7
#define START_PYRKON 8

extern MPI_Datatype MPI_PAKIET_T;
void inicjuj_typ_pakietu();

/* wysyłanie pakietu, skrót: wskaźnik do pakietu (0 oznacza stwórz pusty pakiet), do kogo, z jakim typem */
void sendPacket(packet_t *pkt, int destination, int tag);

// typedef enum {InRun, InMonitor, InWant, InSection, InFinish} state_t;
typedef enum {WaitForPyrkon, TicketWait, OnPyrkon, WorkshopWait, OnWorkshop, OffPyrkon} state_t;
extern state_t stan;
extern pthread_mutex_t stateMut;
extern pthread_mutex_t lamportMut;
/* zmiana stanu, obwarowana muteksem */
void changeState( state_t );
#endif
