#include "main.h"
#include "watek_glowny.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdlib.h>

void mainLoop()
{
    int shmid;
    char* data;
    pthread_mutex_t licznikMut = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&licznikMut);
    /*  create the segment: */
    if ((shmid = shmget(123456, sizeof(int), 0644 | IPC_CREAT)) == -1) {
        perror("shmget");
        exit(1);
    }

    /* attach to the segment to get a pointer to it: */
    if ((data = shmat(shmid, NULL, 0)) == (void *)-1) {
        perror("shmat");
        exit(1);
    }

    /* read or modify the segment, based on the rank: */ /*
    if (rank == 0) {
        println("writing to segment: \"%d\"\n", 69);
        sprintf(data, "%d", 69);
    } else
        println("segment contains: \"%s\"\n", data);

    */

    if(rank == 0){
        sprintf(data, "%d", 0);
    }
    println("Segment zawiera: %s", data);

    /* detach from the segment: */
    /*if (shmdt(data) == -1) {
        perror("shmdt");
        exit(1);
    }*/
    pthread_mutex_unlock(&licznikMut);
    srandom(rank);
    int tag;
    int perc;
    //packet_t *pyrPkt = (packet_t*)malloc(sizeof(packet_t));
    packet_t *pkt = (packet_t*)malloc(sizeof(packet_t));
    println("Dostępność w tej edycji: %d bilety, %d warsztaty z %d miejscami\n", ticketCount, workshopCount, workshopMemberCount);
    int flag = 1, printFlag = 1;
    while (flag) {
	switch (stan) {
	    case WaitForPyrkon:
                for(int i = 0; i < size; ++i){
                    pyrkonLamportValues[i] = 2147483647;
                }
                for(int i = 0; i < size; ++i){
                    workshopLamportValues[i] = 2147483647;
                }
                //pyrPkt = (packet_t*)malloc(sizeof(packet_t));
                printFlag = 1;
		perc = random()%100;
		if ( perc < 25 ) {
                    TicketWaiting = (int*)malloc(sizeof(int) * 0);
                    TicketWaiterNum = 0;
                    WorkshopWaiting = (int*)malloc(sizeof(int) * 0);
                    WorkshopWaiterNum = 0;
                    changeState( TicketWait );
		    debug("Perc: %d", perc);
		    println("Ubiegam się o wstęp na Pyrkon %d", edition)
		    debug("Zmieniam stan na wysyłanie");
		    //packet_t *pkt = malloc(sizeof(packet_t));
		    pkt->data = perc;
		    TicketAckNum = 0;
                    pthread_mutex_lock(&lamportMut);
		    for (int i=0;i<=size-1;i++){
			if (i!=rank){
			    sendPacket( pkt, i, TICKET_REQ);
                        }
                        pyrkonLamportValues[i] = lamport;
                        debug("Wyslalem do %d prośbę o czasie %d", i, pyrkonLamportValues[i]);
                    }
                    pthread_mutex_unlock(&lamportMut);
		    //changeState( TicketWait ); // w VI naciśnij ctrl-] na nazwie funkcji, ctrl+^ żeby wrócić
					   // :w żeby zapisać, jeżeli narzeka że w pliku są zmiany
					   // ewentualnie wciśnij ctrl+w ] (trzymasz ctrl i potem najpierw w, potem ]
					   // między okienkami skaczesz ctrl+w i strzałki, albo ctrl+ww
					   // okienko zamyka się :q
					   // ZOB. regułę tags: w Makefile (naciśnij gf gdy kursor jest na nazwie pliku)
		    //free(pkt);
		} // a skoro już jesteśmy przy komendach vi, najedź kursorem na } i wciśnij %  (niestety głupieje przy komentarzach :( )
		debug("Skończyłem myśleć");
		break;
	    case TicketWait:
		println("Czekam na wejście na Pyrkon %d", edition)
		// tutaj zapewne jakiś semafor albo zmienna warunkowa
		// bo aktywne czekanie jest BUE
		if ( TicketAckNum >= size - ticketCount){
                    changeState(OnPyrkon);
                    currentlySoughtAfter = 0;
                    pthread_mutex_lock(&licznikMut);
                    sprintf(data, "%d", atoi(data) + 1);
                    println("No to mamy %s ludzi teraz na Pyrkonie", data);
                    pthread_mutex_unlock(&licznikMut);
                    wantedWorkshopCount = rand()%workshopCount;
                    if(wantedWorkshopCount == 0)
                        wantedWorkshopCount = 1;
                    WantedWorkshops = (int*)malloc(sizeof(int) * wantedWorkshopCount);
                    int verifyUniqueness;
                    for(int i = 0; i < wantedWorkshopCount; ++i){
                        WantedWorkshops[i] = rand()%workshopCount;
                        verifyUniqueness = 1;
                        while(verifyUniqueness){
                            verifyUniqueness = 0;
                            for(int j = 0; j < i; ++j){
                                if(WantedWorkshops[i] == WantedWorkshops[j]){
                                    WantedWorkshops[i] = rand()%workshopCount;
                                    verifyUniqueness = 1;
                                    break;
                                }
                            }
                        }
                    }
                    debug("Wylosowało mi się %d warsztatów, a oto one:", wantedWorkshopCount);
                    for(int i = 0; i < wantedWorkshopCount; ++i){
                        debug("Warsztat %d: %d", i+1, WantedWorkshops[i]);
                    }
                }
		break;
	    case OnPyrkon:
		// tutaj zapewne jakiś muteks albo zmienna warunkowa
                if(currentlySoughtAfter == 0){
		    println("Wszedłem na Pyrkon %d!", edition);
                }
                else{
                    println("Wróciłem na Pyrkon %d!", edition);
                }
		    //sleep(rank * 2 + 5);
		//if ( perc < 25 ) {
                //currentlySoughtAfter = 250;
		    debug("Perc: %d", perc);
                    if(currentlySoughtAfter < wantedWorkshopCount){
                        WorkshopAckNum = 0;
                        //debug("Przed alokacją pkt");
                        //packet_t* pyrPkt = (packet_t*)malloc(sizeof(packet_t));
                        //debug("Po alokacji pkt");
                        pkt->data = WantedWorkshops[currentlySoughtAfter];
                        changeState(WorkshopWait);
                        pthread_mutex_lock(&lamportMut);
                        for (int i=0;i<=size-1;++i){
                            if (i != rank){
                                sendPacket( pkt, i%size, WORKSHOP_REQ);
                                workshopLamportValues[i] = lamport;
                            }
                        }
                        pthread_mutex_unlock(&lamportMut);
                        //free(pyrPkt);
                    }
                    else{
		        println("Wychodzę z Pyrkonu %d", edition)
                        pthread_mutex_lock(&licznikMut);
                        sprintf(data, "%d", atoi(data) - 1);
                        println("Zatem teraz na Pyrkonie jest %s osób", data);
                        pthread_mutex_unlock(&licznikMut);
		        debug("Zmieniam stan na wysyłanie");
		        //packet_t *pkt = malloc(sizeof(packet_t));
		        pkt->data = perc;
		        /*for (int i=0;i<=size-1;i++)
			    if (i!=rank)
			        sendPacket( pkt, (i+1)%size, TICKET_ACK);*/
                        debug("I w czasie, jak tam byłem, takie typki mi się zapisały:");
                        for(int i = 0; i < TicketWaiterNum; ++i){
                            debug("Typek %d: %d. Wyślę mu ACKa.", i+1, TicketWaiting[i]);
                            sendPacket(pkt, TicketWaiting[i], TICKET_ACK);
                        }
		        changeState( OffPyrkon );
		        //free(pkt);
                        free(WantedWorkshops);
                        free(TicketWaiting);
                        free(WorkshopWaiting);
                        EndNum = 0;
                        EndResp = 0;
                        for (int i=0;i<=size-1;++i){
                            if (i != rank){
                                sendPacket( pkt, i%size, END_REQ);
                            }
                        }
                    }
		//}
		break;
            case WorkshopWait:
                println("Czekam na wejście na %d z kolei Warsztat %d", currentlySoughtAfter, WantedWorkshops[currentlySoughtAfter]);
		// tutaj zapewne jakiś semafor albo zmienna warunkowa
		// bo aktywne czekanie jest BUE
		if ( WorkshopAckNum >= size - workshopMemberCount){
                    println("Wchodzę na warsztat %d", WantedWorkshops[currentlySoughtAfter]);
                    changeState(OnWorkshop);
                    //sprintf(data, "%d", atoi(data) + 1);
                    //println("No to mamy %s ludzi teraz na warsztacie", data);
                }
                break;
            case OnWorkshop:
                sleep(1);
	        println("Wychodzę z Warsztatu %d", WantedWorkshops[currentlySoughtAfter]);
	        debug("Zmieniam stan na wysyłanie");
	        //packet_t *pkt = malloc(sizeof(packet_t));
	        pkt->data = perc;
	        /*for (int i=0;i<=size-1;i++)
		    if (i!=rank)
	               sendPacket( pkt, (i+1)%size, TICKET_ACK);*/
                debug("I w czasie, jak tam byłem, takie typki mi się zapisały:");
                for(int i = 0; i < WorkshopWaiterNum; ++i){
                    debug("Typek %d: %d. Wyślę mu ACKa.", i+1, WorkshopWaiting[i]);
                    sendPacket(pkt, WorkshopWaiting[i], WORKSHOP_ACK);
                }
                //free(pkt);
                WorkshopWaiterNum = 0;
                WorkshopWaiting = realloc(WorkshopWaiting, sizeof(int) * 0);
                for(int i = 0; i < size; ++i){
                    workshopLamportValues[i] = 2147483647;
                }
                changeState(OnPyrkon);
                currentlySoughtAfter++;
                break;
            case OffPyrkon:
                //free(pyrPkt);
//                if(printFlag){
//                    println("Wyszedlem z Pyrkonu.");
//                    printFlag = 1;
//                }
                if(EndResp >= size - 1){
                   if(EndNum >= size - 1){
                       println("BYLEM OSTATNI w %d!! JA ZACZYNAM NOWY PYRKON!!", edition);
                       //packet_t *pkt = malloc(sizeof(packet_t));
                       //sprawdzenie, czy w międzyczasie ktoś nie doszedł do tego samego wniosku
                       //jeśli dwa procesy wyjdą w tym samym momencie z Pyrkonu, to dwa procesy stwierdzą, że były ostatnie
                       if(newPyrkon){
                           println("Ale ktoś inny już to ogarnął przede mną!");
                       }
                       else{
                           for (int i = 0; i <= size - 1; ++i){
                               if(i != rank)
                                   sendPacket(pkt, i%size, START_PYRKON);
                           }
                       }
                       newPyrkon = 0;
                       ++edition;
                       //free(pkt);
                       changeState(WaitForPyrkon);
                   }
                   else{
                       if(printFlag){
                           println("Nie bylem ostatni w %d :(", edition);
                       }
                       if(newPyrkon){
                           println("O, ogłosili nowy Pyrkon!");
                           newPyrkon = 0;
                           ++edition;
                           changeState(WaitForPyrkon);
                       }
                       else{
                           if(printFlag){
                               println("A nowego Pyrkonu dalej nie ma T_T");
                               printFlag = 0;
                           }
                       }
                   }
                }
                break;
	    default:
		break;
            }
        sleep(SEC_IN_STATE);
    }
}
